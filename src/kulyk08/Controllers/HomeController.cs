﻿using Microsoft.AspNetCore.Mvc;

namespace kulyk08
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var students = LoadStud.students;

            return View(students);
        }
    }
}
