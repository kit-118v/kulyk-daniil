﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace kulyk08
{
    public class Student
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime EnterDate { get; set; }
        public string GroupIndex { get; set; }
        public string Faculty { get; set; }
        public string Specialization { get; set; }
        public int Performance { get; set; }

        public Student()
        {
            LastName = "Kulyk";
            FirstName = "Daniil";
            Patronymic = "Ihorovich";
            BirthDate = new DateTime(2000, 7, 20);
            EnterDate = new DateTime(2018, 9, 1);
            GroupIndex = "v";
            Faculty = "CIT";
            Specialization = "Computer engineering";
            Performance = 60;
        }
        public Student(string newLastName, string newFirstName, string newPatronymic, DateTime newBirthDate,
            DateTime newEnterDate, string newGroupIndex, string newFaculty, string newSpecialization, int newPerformance)
        {
            LastName = newLastName;
            FirstName = newFirstName;
            Patronymic = newPatronymic;
            BirthDate = newBirthDate;
            EnterDate = newEnterDate;
            GroupIndex = newGroupIndex;
            Faculty = newFaculty;
            Specialization = newSpecialization;
            Performance = newPerformance;
        }

        public int Age(Student stud)
        {
            int year = stud.BirthDate.Year;
            int age = DateTime.Now.Year - year;
            return age;
        }

        public override bool Equals(object obj)
        {
            Student another = (Student)obj;
            return LastName.ToLower().Equals(another.LastName.ToLower()) &&
                FirstName.ToLower().Equals(another.FirstName.ToLower()) &&
                Patronymic.ToLower().Equals(another.Patronymic.ToLower()) &&
                BirthDate.Equals(another.BirthDate) &&
                EnterDate.Equals(another.EnterDate) &&
                GroupIndex.ToLower().Equals(another.GroupIndex.ToLower()) &&
                Faculty.ToLower().Equals(another.Faculty.ToLower()) &&
                Specialization.ToLower().Equals(another.Specialization.ToLower()) &&
                Performance == another.Performance;
        }

        public override string ToString()
        {
            return $"Surname: {LastName}\nName: {FirstName}\nPatronymic: {Patronymic}\nDate of birth: {BirthDate}\nEnter date: {EnterDate}\nGroup index: {GroupIndex}\nFaculty: {Faculty}\nSpecialty: {Specialization}\n" +
               $"Academic performance: {Performance}\n";
        }

        public override int GetHashCode()
        {
            var hash = new HashCode();
            hash.Add(LastName);
            hash.Add(FirstName);
            hash.Add(Patronymic);
            hash.Add(BirthDate);
            hash.Add(EnterDate);
            hash.Add(GroupIndex);
            hash.Add(Faculty);
            hash.Add(Specialization);
            hash.Add(Performance);
            return hash.ToHashCode();
        }
    }
}