﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace kulyk08
{
    public class LoadStud
    {
        public static StudentContainer<Student> students = ReadFile();

        public static StudentContainer<Student> ReadFile()
        {
            StudentContainer<Student> temp = new StudentContainer<Student>();
            string name, lastname, patronymic, faculty, text, groupIndex, specialty;
            DateTime birthDate, enterDate;
            int progress;

            string path = @"C:\tmp\StudentDB.txt";
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    text = sr.ReadToEnd();

                    string[] separatingStrings = { " ", "\r", "\n", ":", "00:00:00", "\t", "Surname", "Name", "Patronymic", "Date of birth",
                                    "Enter date", "Group index", "Faculty", "Specialty", "Academic performance"};
                    string[] words = text.Split(separatingStrings, System.StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < words.Length / 9; i++)
                    {
                        name = words[9 * i + 1];
                        lastname = words[9 * i];
                        patronymic = words[9 * i + 2];
                        birthDate = DateTime.Parse(words[9 * i + 3]);
                        enterDate = DateTime.Parse(words[9 * i + 4]);
                        groupIndex = words[9 * i + 5];
                        faculty = words[9 * i + 6];
                        specialty = words[9 * i + 7];
                        progress = int.Parse(words[9 * i + 8]);
                        var stud = new Student(name, lastname, patronymic, birthDate, enterDate, groupIndex, faculty, specialty, progress);
                        temp.Add(stud);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return temp;
        }
    }
}
