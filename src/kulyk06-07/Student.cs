﻿using System;

namespace kulyk06
{
    public class Student
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime EnterDate { get; set; }
        public string GroupIndex { get; set; }
        public string Faculty { get; set; }
        public string Specialization { get; set; }
        public int Performance { get; set; }

        public Student()
        {
            LastName = "Kulyk";
            FirstName = "Daniil";
            Patronymic = "Ihorovich";
            BirthDate = new DateTime(2000, 7, 20);
            EnterDate = new DateTime(2018, 9, 1);
            GroupIndex = "v";
            Faculty = "CIT";
            Specialization = "Computer engineering";
            Performance = 96;
        }
        public Student(string newLastName, string newFirstName, string newPatronymic, DateTime newBirthDate,
            DateTime newEnterDate, string newGroupIndex, string newFaculty, string newSpecialization, int newPerformance)
        {
            LastName = newLastName;
            FirstName = newFirstName;
            Patronymic = newPatronymic;
            BirthDate = newBirthDate;
            EnterDate = newEnterDate;
            GroupIndex = newGroupIndex;
            Faculty = newFaculty;
            Specialization = newSpecialization;
            Performance = newPerformance;
        }

        public override string ToString()
        {

            return $"{LastName}|{FirstName}|{Patronymic}|{BirthDate.ToString("d")}|" +
                $"{EnterDate.ToString("d")}|{GroupIndex}|{Faculty}|{Specialization}|{Performance}"; ;
        }

    }
}

