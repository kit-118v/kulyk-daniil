﻿using System.Text.RegularExpressions;

namespace kulyk06
{
    class Validator
    {
        public static string NamePattern = @"^[A-Z][a-z]*$";
        public static string WordsPattern = @"\b[^\d\W]+\b$";

        public static bool ValidateName(string name)
        {
            return Regex.IsMatch(name.Trim(), NamePattern);
        }

        public static bool ValidateString(string value)
        {
            return Regex.IsMatch(value.Trim(), WordsPattern);
        }

        public static bool ValidateInt(int min, int max, int value)
        {
            return value >= min && value <= max;
        }
    }
}
