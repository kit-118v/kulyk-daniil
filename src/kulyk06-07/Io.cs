﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace kulyk06
{
    public static class Io
    {
        private static string _errorMessage = "Invalid input of students data. Please check and try again";

        public static string InputName(string fieldName)
        {
            Console.WriteLine("Enter " + fieldName + ":");
            string name = Console.ReadLine();

            while (!Validator.ValidateName(name))
            {
                Console.WriteLine(_errorMessage);
                name = Console.ReadLine();
            }

            return name;
        }

        public static DateTime InputDate(String fieldName)
        {
            Console.WriteLine("Enter " + fieldName + ":");

            while (true)
            {
                try
                {
                    Console.WriteLine("Enter year:");
                    int year = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter month:");
                    int month = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter day:");
                    int day = Convert.ToInt32(Console.ReadLine());
                    return new DateTime(year, month, day);
                }
                catch (Exception e)
                {
                    Console.WriteLine("An exception occured while date input:" + e.Message);
                }
            }

        }

        public static string InputString(string fieldName)
        {
            Console.WriteLine("Enter " + fieldName + ":");
            string sentence = Console.ReadLine();

            while (!Validator.ValidateString(sentence))
            {
                Console.WriteLine(_errorMessage);
                sentence = Console.ReadLine();
            }

            return sentence;
        }

        public static int InputInt(string fieldName)
        {
            Console.WriteLine("Enter " + fieldName + ":");
            while (true)
            {
                if (int.TryParse(Console.ReadLine(), out int value))
                {
                    return value;
                }

                Console.WriteLine("Error: wrong type");
            }
        }
        public static Student Insert()
        {
            var newStudent = new Student();

            newStudent.FirstName = InputName("first name");
            newStudent.LastName = InputName("last name");
            newStudent.Patronymic = InputName("patronymic");
            newStudent.BirthDate = InputDate("birth date");
            newStudent.EnterDate = InputDate("enrollment date");
            newStudent.GroupIndex = InputString("group index");
            newStudent.Faculty = InputString("faculty name");
            newStudent.Specialization = InputString("specialization");
            newStudent.Performance = InputInt("perfomance");

            return newStudent;
        }

        public static void WriteStContainerToFile(string path, StudentContainer writtenContainer)
        {

            try
            {
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {

                    foreach (var i in writtenContainer)
                    {
                        sw.WriteLine(i.ToString());
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public static void ReadStContainerFromFile(string path, StudentContainer readContainer)
        {
            try
            {
                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var infoStudent = line.Split(new char[] { '|' });

                        readContainer.AddStudent(new Student(infoStudent[0],
                            infoStudent[1],
                            infoStudent[2],
                            DateTime.Parse(infoStudent[3]),
                            DateTime.Parse(infoStudent[4]),
                            infoStudent[5],
                            infoStudent[6],
                            infoStudent[7],
                            int.Parse(infoStudent[8])));
                    }
                }
            }
            catch (Exception e) { }
        }

        public static void Save(StudentContainer data)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(Student[]));

            using (FileStream fs = new FileStream("StudentDB.xml", FileMode.OpenOrCreate))
            {
                formatter.Serialize(fs, data.Students);
            }

        }

        public static void Download(StudentContainer data)
        {
            XmlSerializer formatter = new XmlSerializer(typeof(Student[]));
            using (FileStream filestream = new FileStream("StudentDB.xml", FileMode.OpenOrCreate))
            {
                data.Students = (Student[])formatter.Deserialize(filestream);
            }

        }

        public static void OutputStudent(Student printedStudent)
        {
            Console.WriteLine(printedStudent);
        }

    }
}

