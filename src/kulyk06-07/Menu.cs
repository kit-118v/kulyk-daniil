﻿using System;
using System.Linq;
using System.Text;

namespace kulyk06
{
    public static class Menu
    {
        delegate void Message();
        public static void Start()
        {
            Message mes = PrintMenu;
            int tmpIndex;
            int choice = 1;
            var students = new StudentContainer();
            Student tmpStudent;
            const string path = @"C:\tmp\StudentDB.txt";

            Console.WriteLine("University student database");

            while (choice != 0)
            {
                mes();
                Console.Write("Make a choice: ");
                choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        students.ShowAll();
                        break;
                    case 2:
                        tmpStudent = Io.Insert();
                        students.AddStudent(tmpStudent);
                        break;
                    case 3:
                        tmpIndex = Io.InputInt("index to delete element");
                        students.DeleteStudent(tmpIndex);
                        break;
                    case 4:
                        tmpIndex = Io.InputInt("index to show element");
                        students.ShowByIndex(tmpIndex);
                        break;
                    case 5:
                        students.Sort();
                        break;
                    case 6:
                        Io.WriteStContainerToFile(path, students);
                        break;
                    case 7:
                        Io.ReadStContainerFromFile(path, students);
                        break;
                    case 8:
                        tmpIndex = Io.InputInt("index to edit element");
                        students.EditByIndex(tmpIndex);
                        break;
                    case 9:
                        SpecialOut(students);
                        break;
                    case 10:
                        Io.Save(students);
                        break;
                    case 11:
                        Io.Download(students);
                        break;
                }
            }

        }

        public static void SpecialOut(StudentContainer data)
        {
            int choice = 1, choice2 = 1;
            int tmpIndex;
            StringBuilder sb = new StringBuilder();
            String temp;

            while (choice != 0)
            {
                Menu.PrintSpecialMenu();
                Console.Write("Make a choice: ");
                choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Choose:\n");
                        Console.WriteLine("1 - By faculty");
                        Console.WriteLine("2 - By specialty");
                        Console.WriteLine("3 - By group");
                        choice2 = Io.InputInt("Choise");
                        switch (choice2)
                        {
                            case 1:
                                temp = Io.InputString("Faculty");
                                var selectedItems1 = from t in data.Students
                                                     where t.Faculty.Equals(temp)
                                                     select t.Faculty + t.Specialization + "-" + t.BirthDate.Year + t.GroupIndex;

                                Console.WriteLine("Group of students: ");
                                foreach (string s in selectedItems1)
                                    Console.Write(s + "  ");
                                Console.WriteLine();

                                break;
                            case 2:
                                temp = Io.InputString("GroupIndex");
                                var selectedItems2 = from t in data.Students
                                                     where t.GroupIndex.Equals(temp)
                                                     select t.Faculty + t.Specialization + "-" + t.BirthDate.Year + t.GroupIndex;

                                Console.WriteLine("Group of students: ");
                                foreach (string s in selectedItems2)
                                    Console.Write(s + "  ");
                                Console.WriteLine();
                                break;

                            case 3:
                                temp = Io.InputString("Specialization");
                                var selectedItems3 = from t in data.Students
                                                     where t.Specialization.Equals(temp)
                                                     select t.Faculty + t.Specialization + "-" + t.BirthDate.Year + t.GroupIndex;

                                Console.WriteLine("Group of students: ");
                                foreach (string s in selectedItems3)
                                    Console.Write(s + "  ");
                                Console.WriteLine();
                                break;

                        }
                        break;
                    case 2:
                        tmpIndex = Io.InputInt("index of student");
                        Console.WriteLine(Calculations.CalCourse(data.Students[tmpIndex]));
                        break;
                    case 3:
                        tmpIndex = Io.InputInt("index of student");

                        Console.WriteLine(Calculations.CalAgeFull(data.Students[tmpIndex]));
                        break;
                    case 4:
                        Console.WriteLine("Choose:\n");
                        Console.WriteLine("1 - By group");
                        Console.WriteLine("2 - By specialty");
                        Console.WriteLine("3 - By faculty");
                        choice2 = Io.InputInt("Choise");
                        Console.WriteLine("Average age: " + Calculations.
                            AverageAge(choice2, Io.InputString("Group|Faculty|Specialization"), data.Students));

                        break;
                    case 5:
                        Console.WriteLine("Choose:\n");
                        Console.WriteLine("1 - By group");
                        Console.WriteLine("2 - By specialty");
                        Console.WriteLine("3 - By faculty");
                        choice2 = Io.InputInt("Choise");
                        Console.WriteLine("Average perfomance: " + Calculations.
                            AveragePerformance(choice2, Io.InputString("Group|Faculty|Specialization"), data.Students));

                        break;

                    case 6:
                        Console.WriteLine("Choose:\n");
                        Console.WriteLine("1 - By group");
                        Console.WriteLine("2 - By specialty");
                        Console.WriteLine("3 - By faculty");
                        choice2 = Io.InputInt("Choise");
                        data.GroupDelete(choice2, Io.InputString("Group|Faculty|Specialization"));

                        break;
                    case 7:
                        temp = Io.InputString("Faculty");
                        var selectedStud1 = (from t in data.Students
                                             where t.Faculty.Equals(temp)
                                             select Calculations.GetAge(t.BirthDate)).Min();

                        Console.WriteLine($"Min age of student is {selectedStud1}");
                        break;
                    case 8:
                        var selectedStudents = (from t in data.Students
                                                where Calculations.GetAge(t.BirthDate) > 20
                                                orderby t.FirstName
                                                select t);
                        foreach (var t in selectedStudents)
                        {
                            Console.WriteLine($"{t.FirstName} {t.LastName} {Calculations.GetAge(t.BirthDate)}");
                        }
                        break;
                }
            }
        }

        public static void PrintSpecialMenu()
        {
            Console.WriteLine("Choose what you want to do:\n");
            Console.WriteLine("1 - Output by group, specialization or faculty");
            Console.WriteLine("2 - Course and semestr till the moment");
            Console.WriteLine("3 - Age of student");
            Console.WriteLine("4 - Average age");
            Console.WriteLine("5 - Average Performance");
            Console.WriteLine("6 - Group delete");
            Console.WriteLine("7 - Get min age of students");
            Console.WriteLine("8 - Get student in a range of 20s");
            Console.WriteLine("\n0 - Exit");
        }
        private static void PrintMenu()
        {
            Console.WriteLine("Choose what you want to do:\n");
            Console.WriteLine("1 - Show data for all students");
            Console.WriteLine("2 - Add new student");
            Console.WriteLine("3 - Delete student by index");
            Console.WriteLine("4 - Show by index");
            Console.WriteLine("5 - Sort");
            Console.WriteLine("6 - Write to file");
            Console.WriteLine("7 - Read from file");
            Console.WriteLine("8 - Edit student by index");
            Console.WriteLine("9 - Special output");
            Console.WriteLine("10 - Save");
            Console.WriteLine("11 - Download");
            Console.WriteLine("\n0 - Exit");
        }
    }
}
