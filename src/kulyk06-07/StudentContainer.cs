﻿using System;
using System.Collections;

namespace kulyk06
{
    public class StudentContainer : IEnumerable
    {
        public int Size { get; set; }
        public Student[] Students { get; set; }

        public StudentContainer()
        {
            Size = 0;
            Students = new Student[0];
        }

        public void AddStudent(Student newStudent)
        {
            var tmpStudentArray = new Student[Size + 1];

            for (int i = 0; i < Size; i++)
            {
                tmpStudentArray[i] = Students[i];
            }

            tmpStudentArray[Size] = newStudent;

            Students = tmpStudentArray;

            Size++;

            tmpStudentArray = null;
        }

        public void DeleteStudent(int index)
        {
            if (!(index >= 0 && index <= Students.Length - 1))
            {
                return;
            }

            var tmpStudentArray = new Student[Size - 1];

            for (int i = 0; i < index; i++)
            {
                tmpStudentArray[i] = Students[i];
            }

            for (int i = index + 1; i < Size; i++)
            {
                tmpStudentArray[i - 1] = Students[i];
            }

            Students = tmpStudentArray;
            Size--;
            tmpStudentArray = null;
        }

        public void GroupDelete(int number, String str)
        {
            switch (number)
            {
                case 1:

                    for (int i = 0; i < Size; i++)
                    {
                        if (Students[i].GroupIndex.Equals(str))
                        {
                            DeleteStudent(i);
                        }
                    }
                    break;

                case 2:

                    for (int i = 0; i < Size; i++)
                    {
                        if (Students[i].Specialization.Equals(str))
                        {
                            DeleteStudent(i);
                        }
                    }

                    break;
                case 3:

                    for (int i = 0; i < Size; i++)
                    {
                        if (Students[i].Faculty.Equals(str))
                        {
                            DeleteStudent(i);
                        }
                    }

                    break;
            }
        }


        public void ShowByIndex(int index)
        {
            if (index >= Size || index < 0)
            {
                return;
            }

            Io.OutputStudent(Students[index]);
        }

        public void Sort()
        {
            Student temp;

            for (int write = 0; write < Students.Length; write++)
            {
                for (int sort = 0; sort < Students.Length - 1; sort++)
                {
                    if (String.Compare(Students[sort].LastName, Students[sort + 1].LastName) > 0)
                    {
                        temp = Students[sort + 1];
                        Students[sort + 1] = Students[sort];
                        Students[sort] = temp;
                    }
                }
            }

        }
        public void ShowAll()
        {
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("|  №  |     Surname     |      Name       |    Patronymic    |  Date of birth  |  Enter date  |  Group index  |            Faculty            |          Specialty        |    Academic performance   |");
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            int i = 0;

            foreach (Student student in Students)
            {
                String output = String.Format("| {0,-3} | {1,-15} | {2,-15} | {3,-16} | {4,-15} | {5,-12} | {6,-13} | {7,-29} | {8,-25} | {9,-25} |",
                    (++i), student.LastName, student.FirstName, student.Patronymic, student.BirthDate.ToString("dd.MM.yyyy"), student.EnterDate.ToString("dd.MM.yyyy"), student.GroupIndex, student.Faculty, student.Specialization, student.Performance);
                Console.WriteLine(output);
            }
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

        }

        public void EditByIndex(int index)
        {

            int day;
            int month;
            int year;

            int choice = 1;
            Console.WriteLine("Choose what you want to do:\n");
            Console.WriteLine("1 - Change last name");
            Console.WriteLine("2 - Change first name");
            Console.WriteLine("3 - Change Patronymic");
            Console.WriteLine("4 - Change birth date");
            Console.WriteLine("5 - Change enter date");
            Console.WriteLine("6 - Change group index");
            Console.WriteLine("7 - Change faculty");
            Console.WriteLine("8 - Change specialization");
            Console.WriteLine("8 - Change perfomance");
            choice = Io.InputInt("choise");


            switch (choice)
            {
                case 1:
                    Students[index].LastName = Io.InputName("LastName");
                    break;
                case 2:
                    Students[index].FirstName = Io.InputName("FirstName");
                    break;
                case 3:
                    Students[index].Patronymic = Io.InputName("Patronymic");
                    break;
                case 4:
                    day = Io.InputInt("day");
                    month = Io.InputInt("month");
                    year = Io.InputInt("year");
                    Students[index].BirthDate = new DateTime(year, month, day);
                    break;
                case '5':
                    day = Io.InputInt("day");
                    month = Io.InputInt("month");
                    year = Io.InputInt("year");
                    Students[index].EnterDate = new DateTime(year, month, day);
                    break;
                case '6':
                    Students[index].GroupIndex = Io.InputString("GroupIndex");
                    break;
                case '7':
                    Students[index].Faculty = Io.InputString("Faculty");
                    break;
                case '8':
                    Students[index].Specialization = Io.InputString("Specialization");
                    break;
                case '9':
                    Students[index].Performance = Io.InputInt("Performance");
                    break;
            }
        }

        public IEnumerator GetEnumerator()
        {
            if (Students != null)
            {
                for (int index = 0; index < Students.Length; index++)
                {
                    yield return Students[index];
                }
            }

        }
    }
}
