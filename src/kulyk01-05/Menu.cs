﻿using System;

namespace kulyk01
{
    public static class Menu
    {
        delegate void Message();
        public static void Start()
        {
            Message mes = PrintMenu;
            int tmpIndex;
            int choice = 1;
            var students = new StudentContainer();
            Student tmpStudent;
            const string path = @"C:\tmp\StudentDB.txt";

            Console.WriteLine("University student database");

            while (choice != 0)
            {
                mes();
                Console.Write("Choose an option: ");
                choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        students.ShowAll();
                        break;
                    case 2:
                        tmpStudent = Io.Insert();
                        students.AddStudent(tmpStudent);
                        break;
                    case 3:
                        tmpIndex = Io.InputInt("index to delete element");
                        students.DeleteStudent(tmpIndex);
                        break;
                    case 4:
                        tmpIndex = Io.InputInt("index to show element");
                        students.ShowByIndex(tmpIndex);
                        break;
                    case 5:
                        students.Sort();
                        break;
                    case 6:
                        Io.WriteStContainerToFile(path, students);
                        break;
                    case 7:
                        Io.ReadStContainerFromFile(path, students);
                        break;
                    case 8:
                        tmpIndex = Io.InputInt("index to edit element");
                        students.EditByIndex(tmpIndex);
                        break;
                    case 9:
                        students.SpecialOut();
                        break;
                    case 10:
                        Io.Save(students);
                        break;
                    case 11:
                        Io.Download(students);
                        break;
                }
            }

        }


        public static void PrintSpecialMenu()
        {
            Console.WriteLine("Choose what you want to do:\n");
            Console.WriteLine("1 - Index of group, specialty, faculty");
            Console.WriteLine("2 - Course and semestr till the moment");
            Console.WriteLine("3 - Age of student");
            Console.WriteLine("4 - Average age");
            Console.WriteLine("5 - Average performance");
            Console.WriteLine("6 - Group delete");
            Console.WriteLine("\n0 - Exit");
        }
        private static void PrintMenu()
        {
            Console.WriteLine("Choose what you want to do:\n");
            Console.WriteLine("1 - Show data for all students");
            Console.WriteLine("2 - Add new student");
            Console.WriteLine("3 - Delete student by index");
            Console.WriteLine("4 - Show by index");
            Console.WriteLine("5 - Sort");
            Console.WriteLine("6 - Write to file");
            Console.WriteLine("7 - Read from file");
            Console.WriteLine("8 - Edit student by index");
            Console.WriteLine("9 - Special output");
            Console.WriteLine("10 - Save");
            Console.WriteLine("11 - Download");
            Console.WriteLine("\n0 - Exit");
        }
    }
}
