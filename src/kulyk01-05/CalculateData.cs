﻿using System;
using System.Linq;

namespace kulyk01
{
    public static class CalculateData
    {
        public static String CalAgeFull(Student data)
        {
            DateTime today = DateTime.Today;
            DateTime b = data.BirthDate;
            TimeSpan old = today.Subtract(b);
            var d = new DateTime(old.Ticks);
            int year = Convert.ToInt32(Math.Floor(old.TotalDays / 365));
            int month = Convert.ToInt32(Math.Floor((old.TotalDays % 365) / 31));
            int day = Convert.ToInt32(Math.Floor((old.TotalDays % 365) % 31)) - 2;

            return $"Возраст: {year} лет, {month} месяцев, {day} дней";
        }
        public static String CalCourse(Student data)
        {
            int course, semester;
            course = DateTime.Today.Year - data.EnterDate.Year + 1;
            if (DateTime.Today.Month >= 7 && DateTime.Today.Month <= 12)
            {
                semester = course * 2 - 1;
            }
            else
            {
                semester = course * 2;
            }

            return $"Курс : {course}, семестр : {semester}";
        }
        public static int GetAge(DateTime date)
        {
            DateTime today = DateTime.Today;
            TimeSpan old = today.Subtract(date);
            var d = new DateTime(old.Ticks);

            return d.Year - 1;
        }

        public static int AveragePerfomance(int number, String str, Student[] students)
        {
            int av = 0;

            switch (number)
            {
                case 1:
                    av = (int)(from stud in students where stud.GroupIndex.Equals(str) select stud.Performance).Average();

                    break;

                case 2:
                    av = (int)(from stud in students where stud.Specialization.Equals(str) select stud.Performance).Average();

                    break;

                case 3:
                    av = (int)(from stud in students where stud.Faculty.Equals(str) select stud.Performance).Average();

                    break;

            }
            return av;
        }

        public static int AverageAge(int number, String str, Student[] students)
        {
            int av = 0;

            switch (number)
            {
                case 1:

                    av = (int)(from stud in students where stud.GroupIndex.Equals(str) select GetAge(stud.BirthDate)).Average();

                    break;
                case 2:

                    av = (int)(from stud in students where stud.Specialization.Equals(str) select GetAge(stud.BirthDate)).Average();

                    break;
                case 3:

                    av = (int)(from stud in students where stud.Faculty.Equals(str) select GetAge(stud.BirthDate)).Average();

                    break;
            }

            return av;
        }

    }
}

