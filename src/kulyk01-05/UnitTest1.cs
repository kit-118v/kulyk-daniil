using kulyk01;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace KulykTest
{
    [TestClass]
    public class UnitTest1
    {
       
        [TestMethod]
        public void TestMethod1()
        {
            Assert.AreEqual(CalculateData.GetAge(DateTime.Parse("20.03.2010")), 10);
            Assert.AreEqual(CalculateData.GetAge(DateTime.Parse("14.06.2005")), 15);
            Assert.AreEqual(CalculateData.GetAge(DateTime.Parse("23.08.1997")), 23);
            Assert.AreEqual(CalculateData.GetAge(DateTime.Parse("28.02.2006")), 14);

        }
    }
}
