﻿using System;
using System.Collections;
using System.Text;

namespace kulyk01
{
    public class StudentContainer : IEnumerable
    {
        public int Size { get; set; }
        public Student[] Students { get; set; }

        public StudentContainer()
        {
            Size = 0;
            Students = new Student[0];
        }

        public void AddStudent(Student newStudent)
        {
            var tmpStudentArray = new Student[Size + 1];

            for (int i = 0; i < Size; i++)
            {
                tmpStudentArray[i] = Students[i];
            }

            tmpStudentArray[Size] = newStudent;

            Students = tmpStudentArray;

            Size++;

            tmpStudentArray = null;
        }

        public void DeleteStudent(int index)
        {
            if (!(index >= 0 && index <= Students.Length - 1))
            {
                return;
            }

            var tmpStudentArray = new Student[Size - 1];

            for (int i = 0; i < index; i++)
            {
                tmpStudentArray[i] = Students[i];
            }

            for (int i = index + 1; i < Size; i++)
            {
                tmpStudentArray[i - 1] = Students[i];
            }

            Students = tmpStudentArray;
            Size--;
            tmpStudentArray = null;
        }

        public void GroupDelete(int number, String str)
        {
            switch (number)
            {
                case 1:

                    for (int i = 0; i < Size; i++)
                    {
                        if (Students[i].GroupIndex.Equals(str))
                        {
                            DeleteStudent(i);
                        }
                    }
                    break;

                case 2:

                    for (int i = 0; i < Size; i++)
                    {
                        if (Students[i].Specialization.Equals(str))
                        {
                            DeleteStudent(i);
                        }
                    }

                    break;
                case 3:

                    for (int i = 0; i < Size; i++)
                    {
                        if (Students[i].Faculty.Equals(str))
                        {
                            DeleteStudent(i);
                        }
                    }

                    break;
            }
        }


        public void ShowByIndex(int index)
        {
            if (index >= Size || index < 0)
            {
                return;
            }

            Io.OutputStudent(Students[index]);
        }

        public void Sort()
        {
            Student temp;

            for (int write = 0; write < Students.Length; write++)
            {
                for (int sort = 0; sort < Students.Length - 1; sort++)
                {
                    if (String.Compare(Students[sort].LastName, Students[sort + 1].LastName) > 0)
                    {
                        temp = Students[sort + 1];
                        Students[sort + 1] = Students[sort];
                        Students[sort] = temp;
                    }
                }
            }

        }
        public void ShowAll()
        {
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("|  №  |     Surname     |      Name       |    Patronymic    |  Date of birth  |  Enter date  |  Group index  |            Faculty            |          Specialty        |    Academic performance   |");
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            int i = 0;

            foreach (Student student in Students)
            {
                String output = String.Format("| {0,-3} | {1,-15} | {2,-15} | {3,-16} | {4,-15} | {5,-12} | {6,-13} | {7,-29} | {8,-25} | {9,-25} |",
                    (++i), student.LastName, student.FirstName, student.Patronymic, student.BirthDate.ToString("dd.MM.yyyy"), student.EnterDate.ToString("dd.MM.yyyy"), student.GroupIndex, student.Faculty, student.Specialization, student.Performance);
                Console.WriteLine(output);
            }
            Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

        }

        public void SpecialOut()
        {
            int choice = 1, choice2 = 1;
            int tmpIndex;
            StringBuilder sb = new StringBuilder();


            while (choice != 0)
            {
                Menu.PrintSpecialMenu();
                Console.Write("Make a choice: ");
                choice = Convert.ToInt32(Console.ReadLine());

                switch (choice)
                {
                    case 1:
                        tmpIndex = Io.InputInt("index to edit element");
                        sb.Append(Students[tmpIndex].Faculty);
                        sb.Append(Students[tmpIndex].Specialization);
                        sb.Append("-");
                        sb.Append(Students[tmpIndex].EnterDate.Year);
                        sb.Append(Students[tmpIndex].GroupIndex);
                        sb.AppendLine();
                        Console.WriteLine(sb.ToString());

                        break;
                    case 2:
                        tmpIndex = Io.InputInt("index to edit element");
                        int course, semester;
                        course = DateTime.Today.Year - Students[tmpIndex].EnterDate.Year + 1;
                        if (DateTime.Today.Month >= 7 && DateTime.Today.Month <= 12)
                        {
                            semester = course * 2 - 1;
                        }
                        else
                        {
                            semester = course * 2;
                        }
                        Console.WriteLine($"Курс : {course}, семестр : {semester}");
                        break;
                    case 3:
                        tmpIndex = Io.InputInt("index to edit element");

                        DateTime today = DateTime.Today;
                        DateTime b = Students[tmpIndex].BirthDate;
                        TimeSpan old = today.Subtract(b);
                        var d = new DateTime(old.Ticks);
                        int year = Convert.ToInt32(Math.Floor(old.TotalDays / 365));
                        int month = Convert.ToInt32(Math.Floor((old.TotalDays % 365) / 31));
                        int day = Convert.ToInt32(Math.Floor((old.TotalDays % 365) % 31)) - 2;

                        Console.WriteLine($"Возраст: {year} лет, {month} месяцев, {day} дней");
                        break;
                    case 4:
                        Console.WriteLine("Choose:\n");
                        Console.WriteLine("1 - By group");
                        Console.WriteLine("2 - By specialty");
                        Console.WriteLine("3 - By faculty");
                        choice2 = Io.InputInt("Choise");
                        Console.WriteLine("Average age: " + AverageAge(choice2, Io.InputString("Group|Faculty|Specialty")));

                        break;
                    case 5:
                        Console.WriteLine("Choose:\n");
                        Console.WriteLine("1 - By group");
                        Console.WriteLine("2 - By specialty");
                        Console.WriteLine("3 - By faculty");
                        choice2 = Io.InputInt("Choise");
                        Console.WriteLine("Average perfomance: " + AveragePerfomance(choice2, Io.InputString("Group|Faculty|Specialty")));

                        break;

                    case 6:
                        Console.WriteLine("Choose:\n");
                        Console.WriteLine("1 - By group");
                        Console.WriteLine("2 - By specialty");
                        Console.WriteLine("3 - By faculty");
                        choice2 = Io.InputInt("Choise");
                        GroupDelete(choice2, Io.InputString("Group|Faculty|Specialty"));

                        break;
                }
            }
        }


        public int AverageAge(int number, String str)
        {
            int count = 0, age = 0, av;
            DateTime today = DateTime.Today;
            DateTime b;

            switch (number)
            {
                case 1:

                    for (int i = 0; i < Size; i++)
                    {
                        if (Students[i].GroupIndex.Equals(str))
                        {
                            b = Students[i].BirthDate;
                            TimeSpan old = today.Subtract(b);
                            var d = new DateTime(old.Ticks);
                            count++;
                            age += d.Year - 1;
                        }
                    }

                    break;
                case 2:

                    for (int i = 0; i < Size; i++)
                    {
                        if (Students[i].Specialization.Equals(str))
                        {
                            b = Students[i].BirthDate;
                            TimeSpan old = today.Subtract(b);
                            var d = new DateTime(old.Ticks);
                            count++;
                            age += d.Year - 1;
                        }
                    }

                    break;
                case 3:

                    for (int i = 0; i < Size; i++)
                    {
                        if (Students[i].Faculty.Equals(str))
                        {
                            b = Students[i].BirthDate;
                            TimeSpan old = today.Subtract(b);
                            var d = new DateTime(old.Ticks);
                            count++;
                            age += d.Year - 1;
                        }
                    }

                    break;
            }

            av = age / count;
            return av;
        }

        public int AveragePerfomance(int number, String str)
        {
            int count = 0, progress = 0, av;

            switch (number)
            {
                case 1:

                    for (int i = 0; i < Size; i++)
                    {
                        if (Students[i].GroupIndex.Equals(str))
                        {
                            count++;
                            progress += Students[i].Performance;
                        }
                    }
                    break;

                case 2:

                    for (int i = 0; i < Size; i++)
                    {
                        if (Students[i].Specialization.Equals(str))
                        {
                            count++;
                            progress += Students[i].Performance;
                        }
                    }
                    break;

                case 3:

                    for (int i = 0; i < Size; i++)
                    {
                        if (Students[i].Faculty.Equals(str))
                        {
                            count++;
                            progress += Students[i].Performance;
                        }
                    }
                    break;

            }
            av = progress / count;
            return av;
        }



        public void EditByIndex(int index)
        {

            int day;
            int month;
            int year;

            int choice = 1;
            choice = Io.InputInt("choise");

            switch (choice)
            {
                case 1:
                    Students[index].LastName = Io.InputName("LastName");
                    break;
                case 2:
                    Students[index].FirstName = Io.InputName("FirstName");
                    break;
                case 3:
                    Students[index].Patronymic = Io.InputName("Patronymic");
                    break;
                case 4:
                    day = Io.InputInt("day");
                    month = Io.InputInt("month");
                    year = Io.InputInt("year");
                    Students[index].BirthDate = new DateTime(year, month, day);
                    break;
                case '5':
                    day = Io.InputInt("day");
                    month = Io.InputInt("month");
                    year = Io.InputInt("year");
                    Students[index].EnterDate = new DateTime(year, month, day);
                    break;
                case '6':
                    Students[index].GroupIndex = Io.InputString("GroupIndex");
                    break;
                case '7':
                    Students[index].Faculty = Io.InputString("Faculty");
                    break;
                case '8':
                    Students[index].Specialization = Io.InputString("Specialization");
                    break;
                case '9':
                    Students[index].Performance = Io.InputInt("Performance");
                    break;
            }
        }

        public IEnumerator GetEnumerator()
        {
            if (Students != null)
            {
                for (int index = 0; index < Students.Length; index++)
                {
                    yield return Students[index];
                }
            }

        }
    }
}

